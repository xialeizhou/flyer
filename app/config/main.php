<?php
/**
* @file main.php
* @brief
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/

return array(
    #系统名称
    'app_name'=>'Flyer',
    #路由模式，未启用
    'route_mode'=>'path',
    #默认访问路径
    'default_route'=>array('m'=>'index','c'=>'index','a'=>'index'),
    #全局配置-》模块配置-》控制配置=》动作配置（后面的覆盖前面的）
    'global'=>array(
        'assets_uri'=>'/',
    ),
    #异常报告配置
    'report'=>array(
        'params'=>array(
            'host'=>'0.0.0.0', //地址
            'port'=>7777, //端口
            'status'=>'off', //不使用
            'receiver'=>'flyer' //默认接收者
        )
    ),
    #日志记录配置
    'log'=>array(
        'params'=>array(
            'dir'=>'app.log' //目录
        ),
    ),
    'components'=>array(
        'model'=>array(
            'params'=>array(
                'mysql:host=localhost;dbname=test',
                'flyer',
                'meiyoumima',
                array(),
            )
        ),
        'language'=>array(
            'class'=>'LanguageComponent',
            'params'=>array('type'=>'cn')
        ),
    ),
    'hooks'=>array(
        #'钩子名称'=>array(
        #    array('回调类_不含Hook结尾','回调方法'),
        #),
    )
);
