<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Administrator
 * Date: 13-7-10
 * Time: 下午2:32
 * To change this template use File | Settings | File Templates.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=Bingo::a()->title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?=Bingo::g('assets_uri')?>js/html5shiv.js"></script>
    <![endif]-->
    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?=Bingo::g('assets_uri')?>css/pure-min.css">
    <style type="text/css">
        body{
            background: #ccc url('/img/bg.png') repeat;
        }

        #footer  {
            text-align: center;
            font-family: ​sans-serif;
            font-size: 16px;
            color:#00FF7F ;
            background-color: #fffafa;
            height: 50px;
            /*position:fixed;
            left:0px;
            bottom:0px;*/
        }
        #footer .m-jag {
            background: url("/img/footer-top.png") repeat-x scroll 0 0 transparent;
            display: block;
            height: 6px;
            overflow: hidden;
        }
    </style>
</head>

<body>
<div id="main" class="container">
    <?=$main?>
</div>

<div id="footer" class="pure-u-1">
    <b class="m-jag"></b>
    <p>&copy; 2013 Bingo  All Rights Reserved.</p>
</div>
</body>
</html>