<?php
/**
* @file indexAction.php
* @brief flyer
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/
?>
<style type="text/css">
    #footer{
        height: 98px;
    }
    #top .header{
        border-bottom: 1px solid #ccc;
        color: #333333;
        margin: 0;
        min-height: 80px;
        padding: 1em;
        text-align: center;
        font-family: "Microsoft YaHei","Consolas" ;
    }
    #top .header h1{
        color: #1cd51c;
    }
    #top .header h2{
        color: #cccccc;
        font-size: 18px;
    }
    #features {
        font-family: "Helvetica Neue",Helvetica,Arial,"Microsoft Yahei UI","Microsoft YaHei",SimHei,"宋体",simsun,sans-serif;
        font-weight: normal;
        text-align: center;
        color: #666b69;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #ccc;*/
        margin-bottom: 20px;
    }
    #features .pure-u-1-4{
        width: 24%;
        overflow: hidden;
        font-size: 14px;
    }
    #features .pure-u-1-3 h3{
        font-size: 20px;
    }
    #features .wrap{
        padding:  1.3em;
        /*border: #303030 solid 1px;*/
    }
    #features .wrap div {
        text-align: left;
    }
    #doc {
        position: relative;
        padding-bottom: 20px;
    }
    #docList {
        position: absolute;
        left: 0;
        top: 0;
    }
    #docList .menu-wrap{
        padding-left: 10px;
        padding-right: 10px;
    }
    #docContent {
        float: right;
    }
    #docContent .doc{
        margin: 20px auto;
    }
    #docContent .content {
        border: #ccc solid 1px;
        background-color: #fafafa;
        min-height: 1024px;
        margin-right: 10px;
        padding-left: 10px;
    }
    .pure-clear {
        clear: both;
    }
    #start {
        position:fixed;
        bottom:0;
        height:48px;
        width: 100%;
    }
    #start a {
        letter-spacing: 0.1em;
        display: block;
        width: 100%;
        height:46px;
        line-height: 46px;
        vertical-align: middle;
        border:#fafafa solid 1px;
        background-color: #1CD51C;
        text-decoration: none;
        color: #fafafa;
        font-weight: bold;
        font-size: 18px;
        text-align: center;
        font-family:"Microsoft Yahei UI","Microsoft YaHei",SimHei,"宋体",simsun,sans-serif;
    }
    #start a:hover{
        border: #ccc solid 1px;
        background-color: #44d526;
        color: #fafafa;
    }
</style>
<div id="top" class="pure-u-1">
    <div class="header">
        <h1>Flyer</h1>
        <h2>小巧易用，流程完整，易于拓展，方便维护的PHP开发脚手架</h2>
    </div>
</div>
<div id="features" class="pure-g" >
    <div class="pure-u-1">
        <div class="pure-u-1-4">
            <div class="wrap">
                <h3>小巧易用</h3>
                <div>
                    不像框架那样繁复，简单的骨架结构，提供必须而完整的功能接口。你可以按照你的意愿使用它改变它。
                </div>
            </div>
        </div>
        <div class="pure-u-1-4">
            <div class="wrap">
                <h3>流程完整</h3>
                <div>
                    包含了请求初始化，路由钩子，权限钩子，模块、控制器、动作的前置执行，后置执行，异常日志的记录与报告等一系列完善的执行流程。
                </div>
            </div>
        </div>
        <div class="pure-u-1-4">
            <div class="wrap">
                <h3>易于拓展</h3>
                <div>
                    应用文件优先的覆盖机制，可以按需重写核心文件；钩子机制，可以变更路由，权限判断，改变加载文件的路径、类名、参数，监听异常与错误等，可以方便的拓展。
                </div>
            </div>
        </div>
        <div class="pure-u-1-4">
            <div class="wrap">
                <h3>方便维护</h3>
                <div>
                    简单明了的文件规划，一切都整整有序；以动作为基础，一个动作一个文件，条理清晰；完善的流程，一切都按部就班。
                </div>
            </div>
        </div>
        <!--
        <div class="pure-u-1-3">
            <div class="wrap">
                <h3>维护</h3>
                <div>
                    干净规范的mvc，另外，action单文件结构，避免你做出一盘炒面。
                </div>
            </div>
        </div>
        <div class="pure-u-1-3">
            <div class="wrap">
                <h3>上手</h3>
                <div>
                    好吧，我觉得很容易上手，你说呢？
                </div>
            </div>
        </div>
        -->
    </div>
</div>
<div class="pure-g">
    <div id="doc" class="pure-u-1-1">
        <div id="docList" class="pure-u-1-6">
            <div class="menu-wrap">
                <div class="pure-menu pure-menu-open">
                    <ul>
                        <li><a href="#docDir">目录结构</a></li>
                        <li><a href="#docRule">约定</a></li>
                        <li><a href="#docMvc">mvc</a></li>
                        <li><a href="#docComponents">组件</a></li>
                        <li><a href="#docG">全局变量</a></li>
                        <li><a href="#docGPC">外部变量</a></li>
                        <li><a href="#docLog">日志</a></li>
                        <li><a href="#docReport">报警</a></li>
                        <li><a href="#docHook">钩子</a></li>
                        <li><a href="#docLib">类库</a></li>
                        <li><a href="#docExtensions">拓展</a></li>
                        <li><a href="#docStart">开始</a></li>
                        <li><a href="https://github.com/xialeizhou/flyer" target="_blank">下载</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="pure-u-5-6" id="docContent">
            <div class="content">
                <div class="doc" id="docDir">
                    <h3>目录结构</h3>
                    <div class="info">
                        <dl>
                            <dt>Flyer目录</dt>
                            <dd>
                                <ul>
                                    <li>components:系统组件目录
                                        <ul>
                                            <li>
                                                Model.php:model组件
                                            </li>
                                        </ul>
                                    </li>
                                    <li>config:系统配置目录</li>
                                    <li>core:系统核心文件目录
                                        <ul>
                                            <li>Action.php:动作基类</li>
                                            <li>Flyer.php:系统类</li>
                                            <li>Controller.php：控制基类</li>
                                            <li>Log.php：日志类</li>
                                            <li>Module.php：模块类</li>
                                            <li>Report.php：报警类</li>
                                        </ul>
                                    </li>
                                    <li>extensions:扩展目录，我们引入一些类库可能不符合我们的Flyer::load规范，就在这里写适配</li>
                                    <li>hook:钩子目录</li>
                                    <li>library:类库目录</li>
                                    <li>view:系统视图
                                        <ul>
                                            <li>error:错误默认视图</li>
                                        </ul>
                                    </li>
                                </ul>
                            </dd>
                            <dt>应用目录:</dt>
                            <dd>
                                <ul>
                                    <li>app:应用目录，放在web不能直接访问的位置
                                        <ul>
                                            <li>cache：缓存目录</li>
                                            <li>components：组件目录
                                                <ul>
                                                    <li>LanguageComponent.php：laguage组件,示范组件怎么写的而已</li>
                                                </ul>
                                            </li>
                                            <li>config：配置目录
                                                <ul>
                                                    <li>main.php：主配置文件</li>
                                                </ul>
                                            </li>
                                            <li>controllers：控制器目录
                                                <ul>
                                                    <li>HelloAction.php：无模型无控制器动作,注意和视图不同，类文件的首字母大写</li>
                                                    <li>HelloController：hello控制器目录
                                                        <ul>
                                                            <li>HelloController.php：hello控制器</li>
                                                            <li>IndexAction.php：hello控制器下index动作</li>
                                                        </ul>
                                                    </li>
                                                    <li>HelloModule：hello模块视图目录
                                                        <ul>
                                                            <li>HelloModule.php：hello模块</li>
                                                            <li>HelloController：hello模块hello控制器目录
                                                                <ul>
                                                                    <li>HelloController.php：hello控制器</li>
                                                                    <li>IndexAction.php：hello模块hello控制器下index动作</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>core：核心文件目录，会覆盖掉Flyer的</li>
                                            <li>data：数据目录</li>
                                            <li>extensions：类库适配文件目录</li>
                                            <li>hook：钩子目录</li>
                                            <li>library：类库</li>
                                            <li>log：日志记录</li>
                                            <li>model：模型</li>
                                            <li>view：视图目录，路径：首字母小写，带后缀Module,Controller,Action
                                                <ul>
                                                    <li>layout：这个叫什么好呢
                                                        <ul>
                                                            <li>main.php：主layout</li>
                                                        </ul>
                                                    </li>
                                                    <li>helloAction.php：无模型无控制器动作视图</li>
                                                    <li>helloController：hello控制器视图目录
                                                        <ul>
                                                            <li>indexAction.php：hello控制器下index动作视图</li>
                                                        </ul>
                                                    </li>
                                                    <li>helloModule：hello模块视图目录
                                                        <ul>
                                                            <li>helloController：hello模块hello控制器视图目录
                                                                <ul>
                                                                    <li>indexAction.php：hello模块hello控制器下index动作视图</li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>web:网站根目录
                                        <ul>
                                            <li>index.php:入口文件</li>
                                            <li>other:其它必要的目录，如资源目录等</li>
                                        </ul>
                                    </li>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                </div>

                <div class="doc" id="docRule">
                    <h3>约定</h3>
                    <div class="info">
                        <ol>
                            <li>所有php文件以.php结尾，不使用.class.php等结尾</li>
                            <li>类文件，文件名与类名相同，首字母大写</li>
                            <li>类命名首字母大写，后面带上指明用途的字符,如下：
                                <ol>
                                    <li>Module：模块，如 TestModule.php</li>
                                    <li>Controller：控制器，如 TestController.php</li>
                                    <li>Action：动作，如 TestAction.php</li>
                                    <li>Hook：钩子，如 TestHook.php</li>
                                    <li>Component：组件，如 TestComponent.php</li>
                                    <li>Extension：类库拓展|适配，如 TestExtension.php</li>
                                </ol>
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="doc" id="docMvc">
                    <h3>MVC</h3>
                    <div class="info">
                          <dl>
                              <dt>目录结构</dt>
                              <dd>参见<a href="#docDir">目录结构</a></dd>
                              <dt>获取</dt>
                              <dd>
                                  在全局范围内:<br>
                                  可以通过Flyer::m()获取一个模块，如果有的话<br>
                                  可以通过Flyer::c()获取一个控制器，如果有的话<br>
                                  可以通过Flyer::a()获取一个动作，如果有的话<br>
                                  在控制器里：<br>
                                  可以通过 $this->_m 获取模块<br>
                                  在动作里：<br>
                                  可以通过 $this->_m 获取模块<br>
                                  可以通过 $this->_c 获取控制器<br>
                              </dd>
                              <dt>执行流程</dt>
                              <dd>
                                  <dl>
                                      <dt>模块</dt>
                                      <dd>先执行$this->_before()再调用控制器，最后执行$this->_after(),你可以在模块类里实现这两个方法。</dd>
                                      <dt>控制器</dt>
                                      <dd>先执行$this->_before()再调用动作，最后执行$this->_after(),你可以在控制器类里实现这两个方法。</dd>
                                      <dt>动作</dt>
                                      <dd>执行次序如下：
                                          $this->init();#子类实现覆盖，进行一些初始话，例如给验证外部变量的规则赋值等。<br>
                                          处理参数验证 Flyer::validate($this->_validate)，更新全局变量等<br>
                                          $this->_before(); #子类实现覆盖，预处理<br>
                                          $this->_main(); #子类实现覆盖，主处理<br>
                                          处理并刷出视图。<br>
                                          $this->_after();#子类实现覆盖。最后处理<br>
                                  </dl>
                              </dd>
                          </dl>
                    </div>
                </div>

                <div class="doc" id="docComponents">
                    <h3>组件</h3>
                    <div class="info">
                        组件是属于应用的，通过 Flyer::$app->getComponent($name)获取。<br>
                        如果组件需要配置的话，在应用主配置文件的components段配置<br>
                    </div>
                </div>

                <div class="doc" id="docG">
                    <h3>全局变量</h3>
                    <div class="info">
                        全局配置()-》模块配置-》控制配置=》动作配置（后面的覆盖前面的）。<br>
                        全局配置在应用主配置global指定，m、v、c皆是在类属性$_global处指定。<br>
                        可以通过Flyer::g($name)获取，通过Flyer::g($name,$val)设置。<br>
                    </div>
                </div>

                <div class="doc" id="docGPC">
                    <h3>外部变量</h3>
                    <div class="info">
                        在应用初始化时，get,post,cookie就都被删除了，之后可以通过Flyer::param()获取，但是在获取前要先通过Flyer::validate()校验。
                    </div>
                </div>

                <div class="doc" id="docLog">
                    <h3>日志</h3>
                    <div class="info">
                        预先在钩子error上绑定了记录，在脚本退出时有错误信息也会触发日志记录。<br>
                        如果想手动插入日志记录，请自行参考Log类。
                    </div>
                </div>

                <div class="doc" id="docReport">
                    <h3>报警</h3>
                    <div class="info">
                        我认为，一个线上的应用出了异常最好能让相关人员早点知道。所以加入了报警模块，需配合一个报警端使用（我用python写了一个），默认没启用。
                    </div>
                </div>

                <div class="doc" id="docHook">
                    <h3>钩子</h3>
                    <div class="info">
                        系统放置了一些钩子，你也可以自己添加新钩子，或往钩子里添加新监听。<br>
                        Flyer::bindHook($name,$callBack) 新增钩子或添加监听<br>
                        Flyer::hook($name) 触发钩子 <br>
                        钩子监听$callBack调用后返回Flyer::HOOK_BREAK将结束一个hook(排后面的回调就不处理了)<br>
                        钩子实现一般为静态类的方式，放置于应用或Flyer的hook目录下<br>
                        钩子可以在应用主配置文件中进行配置，具体请参考主配置的注释<br>
                        系统预留的钩子:
                        <dl>
                            <dt>error</dt>
                            <dd>当有错误信息时调用</dd>
                            <dt>shutdown</dt>
                            <dd>当脚本退出时调用</dd>
                            <dt>route</dt>
                            <dd>在系统初始化mvc名称时调用，通过变更Flyer::$_routeInfo 里的m、v、c键值达到目的</dd>
                            <dt>config</dt>
                            <dd>在使用Flyer::config方法时调用，通过变更Flyer::$_loadingConf达到目的</dd>
                            <dt>load</dt>
                            <dd>在使用Flyer::load方法时调用，通过变更 Flyer::$_loading['class']和Flyer::$_loading['params']达到目的</dd>
                            <dt>permission</dt>
                            <dd>在调用mvc之前触发，通过变更Flyer::$_hasPermission达到目的。</dd>
                            <dt>withoutPermission</dt>
                            <dd>在调用mvc之前，如果Flyer::$_hasPermission为false将不会调用mvc并触发该事件</dd>
                        </dl>
                    </div>
                </div>

                <div class="doc" id="docLib">
                    <h3>类库</h3>
                    <div class="info">
                        对于符合我们的<a href="#docRule">约定</a>的类库，可以通过Flyer::load引入并实例化，如果你选择的类库并不符合我们的约定，那么可以在extension目录下建立相应的适配类来使用。<br>
                        系统可以使用Flyer::path方法来获取一个句号路径的路径，但是不包含最后的后缀。<br>
                    </div>
                </div>

                <div class="doc" id="docExtensions">
                    <h3>拓展</h3>
                    <div class="info">
                        应用文件的加载优先级高于Flyer文件，你可以在应用下建立文件替换掉Flyer文件。<br>
                        你还可以参考 钩子，类库等条目，以便更好的进行拓展。
                    </div>
                </div>

                <div class="doc" id="docStart">
                    <h3>开始</h3>
                    <div class="info">
                        入口文件真的很简单:
                        <pre>
                            &lt;?
                            require 'flyer/core/Flyer.php';
                            Flyer::run(realpath('../app'));
                        </pre>
                    </div>
                </div>
            </div>
        </div>
        <div class="pure-g pure-u-1-1 pure-clear"></div>
    </div>
</div>
<div id="start" class="pure-g" >
    <div class="pure-u-1-1">
        <a href="https://github.com/xialeizhou/flyer" target="_blank">git@osc 立刻围观Flyer开发预览版 点击免费下载</a>
    </div>
</div>
<script type="text/javascript" src="<?Flyer::g('assets_uri')?>js/jquery.js"></script>
<script type="text/javascript">
    $(function(){
        $(window).scroll( function(){
            var yLen = $(document).scrollTop();
            var topHeight = $('#top').height();
            var featuresHeight = $('#features').height();
            if (yLen>(topHeight+featuresHeight)) {
                $('#docList').css({'position':'fixed','left':'0','top':'20px'});
            } else {
                $('#docList').css({'position':'absolute','left':'0','top':'0'});
            }
        });
        $(window).trigger("scroll");
    });
</script>
