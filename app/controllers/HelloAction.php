<?php
/**
* @file HelloAction.php
* @brief
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
 */
class HelloAction extends Action{
    public function __construct(){
        $this->_hasView = 0;
    }
    protected function _main(){
        echo 'hello world';
    }
}
