<?php
/**
* @file IndexAction.php
* @brief
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
 */
class IndexAction  extends Action{
    public function init(){
        $this->title = '首页-'.Bingo::$name;
        return $this;
    }
}
