#!/usr/bin/env sh
# @file push.sh
# @brief push repo to remote
# @author xialeizhou@gmail.com
# @version 0.1.0
# @date 2014-10-31
#
git add ./*
git commit -m 'update'
git push -u origin master
