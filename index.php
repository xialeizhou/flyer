<?php
/**
* @file index.php
* @brief flyer:w
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/
require 'flyer/core/Flyer.php';
flyer::run(realpath('app'));
