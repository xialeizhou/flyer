<?php
/**
* @file Model.php
* @brief  copy PDO 禁用 PDO::query PDO::exec
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
 */
class Model{
    /**
     * @var null|PDO
     */
    protected $_db = null;
    /**
     * @var null|array
     */
    protected $_stmt = array();

    public function __construct($params){
        $params[3][self::ATTR_ERRMODE] = self::ERRMODE_EXCEPTION;
        $pdoRef = new ReflectionClass('PDO');
        $this->_db = $pdoRef->newInstanceArgs($params);
    }

    public function beginTransaction(){
        $this->_db->beginTransaction();
        return $this;
    }
    public function commit(){
        $this->_db->commit();
        return $this;
    }
    public function errorCode(){
        return $this->_db->errorCode();
    }
    public function errorInfo(){
        return $this->_db->errorInfo();
    }
    public function getDBAttribute($attribute){
        return $this->_db->getAttribute($attribute);
    }
    public function inTransaction(){
        return $this->_db->inTransaction();
    }
    public function lastInsertId($name = NULL){
        return $this->_db->lastInsertId($name);
    }

    public function quote($string,$parameter_type = self::PARAM_STR){
        return $this->_db->quote($string,$parameter_type);
    }

    public function rollBack(){
        $this->_db->rollBack();
        return $this;
    }
    public function setDBAttribute($attribute,$value){
        $this->_db->setAttribute($attribute,$value);
        return $this;
    }
    public function prepare($name,$statement,$opt=array()){
        $this->_stmt[$name] = $this->_db->prepare($statement,$opt);
        return $this;
    }

    public function bindColumn($name,$column,&$param){
        $this->_stmt[$name]->bindColumn($column,&$param);
        return $this;
    }
    public function bindParam($name,$parameter,&$variable,$data_type=self::PARAM_STR){
        $this->_stmt[$name]->bindParam($parameter,&$variable,$data_type);
        return $this;
    }
    public function bindValue($name,$parameter,$value,$data_type = PDO::PARAM_STR){
        $this->_stmt[$name]->bindValue($parameter,$value,$data_type);
        return $this;
    }
    public function closeCursor($name){
        $this->_stmt[$name]->closeCursor();
        return $this;
    }
    public function columnCount($name){
        return $this->_stmt[$name]->columnCount();
    }
    public function debugDumpParams($name){
        return $this->_stmt[$name]->debugDumpParams();
    }
    public function execute($name,$inputParameters = null){
        return $this->_stmt[$name]->execute($inputParameters);
    }
    public function fetch ($name,$fetchStyle = self::FETCH_ASSOC){
        return $this->_stmt[$name]->fetch($fetchStyle);
    }
    public function fetchAll($name,$fetchStyle = self::FETCH_ASSOC){
        return $this->_stmt[$name]->fetchAll($fetchStyle);
    }
    public function fetchColumn($name,$columnNumber = 0){
        return $this->_stmt[$name]->fetchColumn($columnNumber);
    }
    public function fetchObject ($name,$className = "stdClass"){
        return $this->_stmt[$name]->fetchObject($className);
    }
    public function getStmtAttribute ($name,$attribute){
        return $this->_stmt[$name]->getAttribute($attribute);
    }
    public function getColumnMeta ($name,$column){
        return $this->_stmt[$name]->getColumnMeta($column);
    }
    public function nextRowset($name){
        return $this->_stmt[$name]->nextRowSet();
    }
    public function rowCount($name){
        return $this->_stmt[$name]->rowCount();
    }
    public function setStmtAttribute($name,$attribute,$value){
        $this->_stmt[$name]->setAttribute($attribute,$value);
        return $this;
    }
    public function setFetchMode($name,$mode){
        $this->_stmt[$name]->setFetchMode($mode);
        return $this;
    }
    public function delStmt($name){
        unset($this->_stmt[$name]);
    }


    /**
     * PDO 的常量
     */
    const PARAM_BOOL = 5;
    const PARAM_NULL = 0;
    const PARAM_INT = 1;
    const PARAM_STR = 2;
    const PARAM_LOB = 3;
    const PARAM_STMT = 4;
    const PARAM_INPUT_OUTPUT = -2147483648;
    const PARAM_EVT_ALLOC = 0;
    const PARAM_EVT_FREE = 1;
    const PARAM_EVT_EXEC_PRE = 2;
    const PARAM_EVT_EXEC_POST = 3;
    const PARAM_EVT_FETCH_PRE = 4;
    const PARAM_EVT_FETCH_POST = 5;
    const PARAM_EVT_NORMALIZE = 6;
    const FETCH_LAZY = 1;
    const FETCH_ASSOC = 2;
    const FETCH_NUM = 3;
    const FETCH_BOTH = 4;
    const FETCH_OBJ = 5;
    const FETCH_BOUND = 6;
    const FETCH_COLUMN = 7;
    const FETCH_CLASS = 8;
    const FETCH_INTO = 9;
    const FETCH_FUNC = 10;
    const FETCH_GROUP = 65536;
    const FETCH_UNIQUE = 196608;
    const FETCH_KEY_PAIR = 12;
    const FETCH_CLASSTYPE = 262144;
    const FETCH_SERIALIZE = 524288;
    const FETCH_PROPS_LATE = 1048576;
    const FETCH_NAMED = 11;
    const ATTR_AUTOCOMMIT = 0;
    const ATTR_PREFETCH = 1;
    const ATTR_TIMEOUT = 2;
    const ATTR_ERRMODE = 3;
    const ATTR_SERVER_VERSION = 4;
    const ATTR_CLIENT_VERSION = 5;
    const ATTR_SERVER_INFO = 6;
    const ATTR_CONNECTION_STATUS = 7;
    const ATTR_CASE = 8;
    const ATTR_CURSOR_NAME = 9;
    const ATTR_CURSOR = 10;
    const ATTR_ORACLE_NULLS = 11;
    const ATTR_PERSISTENT = 12;
    const ATTR_STATEMENT_CLASS = 13;
    const ATTR_FETCH_TABLE_NAMES = 14;
    const ATTR_FETCH_CATALOG_NAMES = 15;
    const ATTR_DRIVER_NAME = 16;
    const ATTR_STRINGIFY_FETCHES = 17;
    const ATTR_MAX_COLUMN_LEN = 18;
    const ATTR_EMULATE_PREPARES = 20;
    const ATTR_DEFAULT_FETCH_MODE = 19;
    const ERRMODE_SILENT = 0;
    const ERRMODE_WARNING = 1;
    const ERRMODE_EXCEPTION = 2;
    const CASE_NATURAL = 0;
    const CASE_LOWER = 2;
    const CASE_UPPER = 1;
    const NULL_NATURAL = 0;
    const NULL_EMPTY_STRING = 1;
    const NULL_TO_STRING = 2;
    const ERR_NONE = 00000;
    const FETCH_ORI_NEXT = 0;
    const FETCH_ORI_PRIOR = 1;
    const FETCH_ORI_FIRST = 2;
    const FETCH_ORI_LAST = 3;
    const FETCH_ORI_ABS = 4;
    const FETCH_ORI_REL = 5;
    const CURSOR_FWDONLY = 0;
    const CURSOR_SCROLL = 1;
    const MYSQL_ATTR_USE_BUFFERED_QUERY = 1000;
    const MYSQL_ATTR_LOCAL_INFILE = 1001;
    const MYSQL_ATTR_INIT_COMMAND = 1002;
    const MYSQL_ATTR_COMPRESS = 1003;
    const MYSQL_ATTR_DIRECT_QUERY = 1004;
    const MYSQL_ATTR_FOUND_ROWS = 1005;
    const MYSQL_ATTR_IGNORE_SPACE = 1006;
    const MYSQL_ATTR_SSL_KEY = 1007;
    const MYSQL_ATTR_SSL_CERT = 1008;
    const MYSQL_ATTR_SSL_CA = 1009;
    const MYSQL_ATTR_SSL_CAPATH = 1010;
    const MYSQL_ATTR_SSL_CIPHER = 1011;
    const ODBC_ATTR_USE_CURSOR_LIBRARY = 1000;
    const ODBC_ATTR_ASSUME_UTF8 = 1001;
    const ODBC_SQL_USE_IF_NEEDED = 0;
    const ODBC_SQL_USE_DRIVER = 2;
    const ODBC_SQL_USE_ODBC = 1;
    const PGSQL_ATTR_DISABLE_NATIVE_PREPARED_STATEMENT = 1000;
    const PGSQL_TRANSACTION_IDLE = 0;
    const PGSQL_TRANSACTION_ACTIVE = 1;
    const PGSQL_TRANSACTION_INTRANS = 2;
    const PGSQL_TRANSACTION_INERROR = 3;
    const PGSQL_TRANSACTION_UNKNOWN = 4;
}
/**
$m = new Model(array('dsn'=>'mysql:host=localhost;dbname=test','user_name'=>'root','pass_word'=>'aOS327*&^EE1Zh','opt'=>array()));
**/
var_dump(error_get_last());
