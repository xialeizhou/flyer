<?php
/**
* @file Log.php
* @brief 
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
 */

class Log {
    protected $_logDir = '';
    public function __construct($params){
        $this->_logDir = Flyer::path($params['dir']).'/';
    }
    public function error($e){
        return $this->record(implode("\t",array_slice($e,0,-1)),'error','common');
    }
    /**
        * @brief    record 
        *
        * @param    $msg
        * @param    $type
        * @param    $category
        *
        * @return   
     */
    public function record($msg,$type='normal',$category='common.info'){
        $file = $this->_logDir.str_replace('.','/',$category).'/'.$type.'/'.date('Y/m/d').'.log';
        $dir = dirname($file);
        if (!file_exists($dir) and !@mkdir(dirname($file),0777,true)) {
            return false;
        }
        return file_put_contents($file,$msg.PHP_EOL.PHP_EOL,LOCK_EX|FILE_APPEND);
    }
}
