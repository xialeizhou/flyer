<?php
/**
* @file Module.php
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/
class Module{
    protected $_global = array();
    public function __construct(){
        foreach ($this->_global as $k=>$v) {
            Flyer::g($k,$v);
        }
    }
    protected function _before(){

    }
    protected function _after(){

    }
    public function execute(){
        $this->_before();
        Flyer::c($this)->execute();
        $this->_after();
    }
}
