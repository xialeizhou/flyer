<?php
/**
* @file Report.php
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/

/**
 * Class Report
 * 发送报告通知开发人员,配合python端的report进行
 * 配合日志使用，并不保证一定会通知到人
 * @example
 *      $report = new Report();
 *      $report->init('0.0.0.0',7777);
 *      if (!$report->send('xialeizhou',time())) {
 *          echo $report->getError();
 *      } else {
 *          echo '通知成功';
 *      }
 */
class Report {
    public function error($e){
        return $this->send($e['message']);
    }

    /**
     * @var resource $_socket
     */
    protected $_socket = null;
    protected $_error;
    protected $_host;
    protected $_port;
    protected $_status;
    protected $_receiver;
    public function __construct($param){
        $this->_host = $param['host'];
        $this->_port = $param['port'];
        $this->_status = $param['status'];
        $this->_receiver = $param['receiver'];
    }
    /**
    * @brief    _init
    *
    * @param    $host
    * @param    $port
    *
    * @return
    */
    protected function _init($host,$port){
        $this->_socket = @socket_create(AF_INET,SOCK_STREAM,0);
        if ($this->_socket !== false) {
            if (!@socket_connect($this->_socket,$host,$port)) {
                $this->_socket = false;
            }
        }
        if ($this->_socket === false) {
            $this->_error = socket_strerror(socket_last_error());
        }
    }
    /**
    * @brief    send
    *
    * @param    $msg
    * @param    $name
    *
    * @return
    */
    public function send($msg,$name = null){
        if ($this->_status == 'off') {
            return true;
        }
        if (!$this->_socket) {
            $this->_init($this->_host,$this->_port);
        }
        if ($this->_error) {
            return false;
        }
        if (is_null($name)) {
            $name = $this->_receiver;
        }
        $data = json_encode(array('to'=>$name,'msg'=>$msg));
        $rs = socket_write($this->_socket,$data,strlen($data));
        if ($rs === false) {
            $this->_error = socket_strerror(socket_last_error());
        } else {
            socket_recv($this->_socket,$data,1024,MSG_WAITALL);
            if ($data == 'success') {
                $rs = true;
            } else {
                $rs = false;
                $this->_error = '服务端接收失败。';
            }
        }
        return $rs;
    }
    public function getError(){
        return $this->_error;
    }
}
