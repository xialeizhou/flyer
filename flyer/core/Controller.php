<?php
/**
* @file Controller.php
* @brief flyer
* @author xialeizhou@gmail.com
* @version 0.1.0
* @date 2014-10-31
*/

class Controller {
    protected $_m = null;
    /**
    * @brief array
    *
    * @return
    */
    protected $_global = array();
    public function __construct(){
        if (func_num_args() == 1) {
            $this->_m = func_get_arg(0);
        }
        foreach ($this->_global as $k=>$v) {
            Flyer::g($k,$v);
        }
    }
    /**
    * @brief  execute
    *
    * @return
    */
    public function execute(){
        $this->_before();
        if ($this->_m) {
            Flyer::a($this->_m,$this)->init()->execute();
        } else {
            Flyer::a($this->_m)->init()->execute();
        }
        $this->_after();
    }
    /**
    * @brief  _before
    *
    * @return
    */
    protected function _before(){

    }
    protected function _after(){

    }
}
